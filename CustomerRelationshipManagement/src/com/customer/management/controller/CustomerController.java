package com.customer.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.customer.management.entity.Customer;
import com.customer.management.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	
	@RequestMapping("/list")
	public String showCustomers(Model model) {
		
		List<Customer> customers = customerService.listCustomer();
		
		model.addAttribute("Customers",customers);
		
		return "list-customer";
		
	}
	
	@RequestMapping("/showFormForAdd")
	public String saveCustomer(Model model) {
		
		Customer customer = new Customer();
		
		model.addAttribute("Customer",customer);
		
		return "save-customer";
	}
	
	@RequestMapping("/showFormForUpdate")
	public String updateCustomer(@RequestParam("id") int id,Model model){
		
		Customer customer = customerService.findCustomerById(id);
		model.addAttribute("Customer",customer);
		
		return "save-customer";
		
		
	}
	
	@PostMapping("/save")
	public String saveCustomer(@RequestParam("id") int id, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("email") String email) {
		
		
		Customer customer;
		
		if(id!=0) {
			
			customer = customerService.findCustomerById(id);
			customer.setFirstName(firstName);
			customer.setLastName(lastName);
			customer.setEmail(email);
		}else {
			
			customer= new Customer(firstName, lastName,email);
		}
		
		customerService.saveCustomer(customer);
		
		return "redirect:/customer/list";
		
	}
	
	@RequestMapping("/delete")
	public String deleteCustomer(@RequestParam("id") int id) {
		
		customerService.deleteCustomer(id);
		
		return "redirect:/customer/list";
	}
	
	

}

package com.customer.management.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Tarang Gupta
 *
 */

@Controller
public class HelloController {
	
	/**
	 * for redirecting to the Welcome page
	 * 
	 */
	@RequestMapping("/")
	public String  showMainPage() {
		
		return "index";
	}

}

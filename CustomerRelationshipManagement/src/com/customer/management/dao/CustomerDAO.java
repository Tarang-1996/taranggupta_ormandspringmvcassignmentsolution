package com.customer.management.dao;

import java.util.List;

import com.customer.management.entity.Customer;

/**
 * 
 * @author Tarang Gupta
 *
 */

public interface CustomerDAO {

	List<Customer> findAll();

	Customer findById(int id);

	void saveCustomer(Customer customer);

	void deleteById(int id);

}
package com.customer.management.dao;



import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.customer.management.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	private SessionFactory sessionFactory;
	private Session session;

	public CustomerDAOImpl(SessionFactory sessionFactory) {

		this.sessionFactory = sessionFactory;

		try {

			this.session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {

			this.session = sessionFactory.openSession();
		}
	}
	
	/**
	 * to list all the customer details
	 * 
	 * @return - list of type customer
	 */

	@Override
	public List<Customer> findAll() {

		Transaction transaction = session.beginTransaction();
		List<Customer> customers = session.createQuery("from Customer").list();
		transaction.commit();

		return customers;

	}
	
	/**
	 * to find the customer by id
	 * 
	 * @param
	 * id - id of the customer
	 * 
	 * @return - Object of type Customer
	 */
	
	@Override
	public Customer findById(int id) {
				
		Transaction transaction= session.beginTransaction();
		Customer customer=session.get(Customer.class, id);
		transaction.commit();
		return customer;
	}
	
	/**
	 * to save the customer
	 * 
	 * @param
	 * Customer - Customer object
	 * 
	 * @return - void
	 */
	
	@Override
	public void saveCustomer(Customer customer) {
		
		Transaction transaction= session.beginTransaction();
		session.save(customer);
		transaction.commit();
	}
	
	/**
	 * to delete the customer
	 * 
	 * @param
	 * id - id of the customer
	 * 
	 * @return - void
	 */
	@Override
	public void deleteById(int id) {
		
		Transaction transaction= session.beginTransaction();
		Customer customer=session.get(Customer.class, id);
		session.delete(customer);
		transaction.commit();
	
	}

}

package com.customer.management.service;

import java.util.List;

import com.customer.management.entity.Customer;

/**
 * 
 * @author Tarang Gupta
 *
 */

public interface CustomerService {

	List<Customer> listCustomer();

	void saveCustomer(Customer customer);

	void deleteCustomer(int id);

	Customer findCustomerById(int id);

}
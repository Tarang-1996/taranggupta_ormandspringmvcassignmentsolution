package com.customer.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.customer.management.dao.CustomerDAO;
import com.customer.management.entity.Customer;

/**
 * 
 * @author Tarang Gupta
 *
 */

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDAO customerDAO;
	
	
	/**
	 * to list all the customer details
	 * 
	 * @return - list of type customer
	 */
	@Override
	public List<Customer> listCustomer() {

		return customerDAO.findAll();
	}
	
	/**
	 * to save the customer
	 * 
	 * @param
	 * Customer - Customer object
	 * 
	 * @return - void
	 */

	@Override
	public void saveCustomer(Customer customer) {

		customerDAO.saveCustomer(customer);
	}

	
	/**
	 * to delete the customer
	 * 
	 * @param
	 * id - id of the customer
	 * 
	 * @return - void
	 */
		
	@Override
	public void deleteCustomer(int id) {
		
		customerDAO.deleteById(id);
	}
	
	/**
	 * to find the customer by id
	 * 
	 * @param
	 * id - id of the customer
	 * 
	 * @return - Object of type Customer
	 */
	@Override
	public Customer findCustomerById(int id) {
		
		return customerDAO.findById(id);
	}

}
